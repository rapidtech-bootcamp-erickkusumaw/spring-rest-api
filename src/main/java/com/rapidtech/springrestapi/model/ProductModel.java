package com.rapidtech.springrestapi.model;


import com.rapidtech.springrestapi.entity.ProductEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductModel {
    private int id;
    private String code;
    private String name;
    private Double price;
    private Integer categoryId;
    private String categoryName;

    public ProductModel(ProductEntity entity){
        BeanUtils.copyProperties(entity, this);
        if(entity.getCategory() != null){
            this.categoryName = entity.getCategory().getName();
        }
    }
    public ProductModel(int id, String name, Double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
}
