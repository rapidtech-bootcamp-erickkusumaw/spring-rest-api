package com.rapidtech.springrestapi.service.Impl;

import com.rapidtech.springrestapi.entity.ProductEntity;
import com.rapidtech.springrestapi.model.ProductModel;
import com.rapidtech.springrestapi.repository.ProductRepo;
import com.rapidtech.springrestapi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
@Slf4j
@Service
public class ProductServiceImpl implements ProductService {
    private ProductRepo repository;

    @Autowired
    public ProductServiceImpl(ProductRepo repository){
        this.repository = repository;
    }

    @Override
    public List<ProductModel> getAll(){
        return this.repository.findAll().stream().map(ProductModel::new)
                .collect(Collectors.toList());
//        List<ProductEntity> result = this.repository.findAll();
//        if(result.isEmpty()){
//            Collections.emptyList();
//        }
//        return result.stream().map(ProductModel::new).collect(Collectors.toList());
    }

    @Override
    public Optional<ProductModel> getById(Integer id) {
        if (id == 0){
            return Optional.empty();
        }
        Optional<ProductEntity> result = this.repository.findById(id);
        return result.map(ProductModel::new);
    }

    @Override
    public Optional<ProductModel> save(ProductModel model) {
        if(model == null) {
            return Optional.empty();
        }
        ProductEntity entity = new ProductEntity(model);
        try {
            this.repository.save(entity);
            return Optional.of(new ProductModel(entity));
        }catch (Exception e){
            log.error("Product save is failed, error: {}", e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public Optional<ProductModel> update(Integer id, ProductModel model) {
        if(id == 0) {
            return Optional.empty();
        }
        ProductEntity result = this.repository.findById(id).orElse(null);
        if(result == null){
            return Optional.empty();
        }
        BeanUtils.copyProperties(model, result);
        try {
            this.repository.save(result);
            return Optional.of(new ProductModel(result));
        }catch (Exception e){
            log.error("Product update is failed, error: {}", e.getMessage());
            return Optional.empty();
        }

    }

    @Override
    public Optional<ProductModel> delete(Integer id) {
        if(id == 0) {
            return Optional.empty();
        }

        ProductEntity result = this.repository.findById(id).orElse(null);
        if(result == null){
            return Optional.empty();
        }

        try {
            this.repository.delete(result);
            return Optional.of(new ProductModel(result));
        }catch (Exception e){
            log.error("Product delete is failed, error: {}", e.getMessage());
            return Optional.empty();
        }

    }
}
