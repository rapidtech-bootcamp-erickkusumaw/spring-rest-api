package com.rapidtech.springrestapi.entity;

import com.rapidtech.springrestapi.model.ProductModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product_tab")
public class ProductEntity {
    @Id
    @TableGenerator(name = "product_id_generator", table = "sequence_tab",
            pkColumnName = "gen_name", valueColumnName = "gen_value",
            pkColumnValue="product_id", initialValue=0, allocationSize=0)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "product_id_generator")
    private Integer id;

    @Column(name = "product_code", length = 20, nullable = false)
    private String Code;

    @Column(name = "name", length =  100, nullable = false)
    private String name;

    @Column(name = "product_price")
    private Double price;

    @Column(name = "category_id", nullable = false)
    private Integer CategoryId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "category_id", insertable = false, updatable = false)
    private CategoryEntity category;

    @Column(name = "supplier_id", nullable = false)
    private Long supplierId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "supplier_id", insertable = false, updatable = false)
    private SupplierEntity supplier;

    @OneToMany(mappedBy = "product")
    private Set<PurchaseOrderDetailEntity> purchaseOrderDetails = new HashSet<>();


//    @Column(name = "created_at")
//    private LocalDateTime createdAt;
//
//    @Column(name = "created_by", length = 20)
//    private String createdBy;
//    @Column(name = "updated_at")
//    private LocalDateTime updatedAt;
//
//    @Column(name = "updated_by", length = 20)
//    private String updatedBy;

    public ProductEntity(ProductModel model){
//        this.id = data.getId();
//        this.name = data.getName();
//        this. = data.getProduct_category();
//        this.price = data.getPrice();
//        this.createdAt = LocalDateTime.now();
//        this.createdBy = "SYSTEM";
//        this.updatedAt = LocalDateTime.now();
//        this.updatedBy = "SYSTEM";
        BeanUtils.copyProperties(model, this);
    }
}
